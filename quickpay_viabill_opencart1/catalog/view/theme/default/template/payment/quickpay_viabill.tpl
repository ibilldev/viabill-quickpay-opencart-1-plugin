<form action="<?php echo $action ?>" method="post" id="vb_quickpay_checkout">
    <input type="hidden" name="version" value="<?php echo $quickpay_viabill_version; ?>">
    <input type="hidden" name="merchant_id" value="<?php echo $quickpay_viabill_merchant; ?>">
    <input type="hidden" name="agreement_id" value="<?php echo $quickpay_viabill_agreement; ?>">
    <input type="hidden" name="language" value="<?php echo $quickpay_viabill_language; ?>">
    <input type="hidden" name="order_id" value="<?php echo $order_id; ?>">
    <input type="hidden" name="amount" value="<?php echo $amount; ?>">
    <input type="hidden" name="currency" value="<?php echo $currency; ?>">
    <input type="hidden" name="continueurl" value="<?php echo $quickpay_viabill_continueurl; ?>">
    <input type="hidden" name="cancelurl" value="<?php echo $quickpay_viabill_cancelurl; ?>">
    <input type="hidden" name="callbackurl" value="<?php echo $quickpay_viabill_callbackurl; ?>">
    <input type="hidden" name="autocapture" value="<?php echo $quickpay_viabill_autocapture; ?>">
    <input type="hidden" name="autofee" value="<?php echo $quickpay_viabill_autofee; ?>">
    <input type="hidden" name="payment_methods" value="<?php echo $quickpay_cardtypelock; ?>">
    <input type="hidden" name="checksum" value="<?php echo $quickpay_checksum; ?>">
</form> 
<div class="buttons">
	<div class="right">
		<a onclick="$('#vb_quickpay_checkout').submit();" class="button">
			<span><?php echo $button_confirm; ?></span>
		</a>
	</div>
</div>