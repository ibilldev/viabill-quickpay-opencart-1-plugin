<?php 
class ModelPaymentQuickPayViabill extends Model {

  	public function getMethod($address) { 
		$this->language->load('payment/quickpay_viabill');
		
		if ($this->config->get('quickpay_viabill_status')) {
      		$status = TRUE;
      	} else {
			$status = FALSE;
		}
		
		$method_data = array();
	
		if ($status) {  
      		$method_data = array( 
        		'code'         => 'quickpay_viabill',
        		'title'      => $this->language->get('text_title'),
				    'sort_order' => $this->config->get('quickpay_viabill_sort_order'),
            'qp_pricetagsrc' => $this->config->get('quickpay_viabill_pricetagsrc')
      		);
    	}



    $total_data = array();          
    $total = 0;
    $taxes = $this->cart->getTaxes();
    
    $this->load->model('setting/extension');
    
    $sort_order = array(); 
    
    $results = $this->model_setting_extension->getExtensions('total');
    
    foreach ($results as $key => $value) {
      $sort_order[$key] = $this->config->get($value['code'] . '_sort_order');
    }
    
    array_multisort($sort_order, SORT_ASC, $results);
    
    foreach ($results as $result) {
      if ($this->config->get($result['code'] . '_status')) {
        $this->load->model('total/' . $result['code']);
  
        $this->{'model_total_' . $result['code']}->getTotal($total_data, $total, $taxes);
      }
    }
  
    $this->load->model('checkout/order');

    $totalsmax = count($total_data)-1;
    $amount = $this->currency->format($total_data[$totalsmax]['value'], $this->session->data['currency'], FALSE, FALSE);
    //======================================================================================================================
    $method_data['priceTotal'] = $amount;
    //======================================================================================================================
    	return $method_data;
  	}
}
?>