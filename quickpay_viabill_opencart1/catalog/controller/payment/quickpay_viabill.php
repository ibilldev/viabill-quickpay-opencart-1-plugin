<?php
class ControllerPaymentQuickPayViabill extends Controller {
	protected function index() {
    	$this->language->load('payment/quickpay_viabill');
		
		$this->data['text_credit_card'] = $this->language->get('text_credit_card');
		$this->data['text_start_date'] = $this->language->get('text_start_date');
		$this->data['text_issue'] = $this->language->get('text_issue');
		$this->data['text_wait'] = $this->language->get('text_wait');
		$this->data['text_viabill_viabill'] = $this->language->get('text_viabill_viabill');
		$this->data['text_method_viabill'] = $this->language->get('text_method_viabill');
		
		$this->data['button_confirm'] = $this->language->get('button_confirm');
		$this->data['button_back'] = $this->language->get('button_back');
		
		$this->data['action'] = 'https://payment.quickpay.net/';
		
		$fields = array(
						'quickpay_viabill_merchant', 
                        'quickpay_viabill_agreement',
						'quickpay_viabill_language', 
						'quickpay_viabill_currency', 
						'quickpay_viabill_order_status_id', 
						'quickpay_viabill_autocapture', 
                        'quickpay_viabill_autofee'
                        );
		
		foreach ($fields as $field) {
			$this->data[$field] = $this->config->get($field);
		}
		
        $this->data['quickpay_viabill_version'] = 'v10';
		$this->data['quickpay_viabill_continueurl'] = $this->url->link('checkout/success', '', 'SSL');
		$this->data['quickpay_viabill_cancelurl'] = $this->url->link('checkout/checkout', '', 'SSL');
		$this->data['quickpay_viabill_callbackurl'] = $this->url->link('payment/quickpay_viabill/callback', '', 'SSL');
		
		$this->data['back'] = $this->data['quickpay_viabill_cancelurl'];
		
		
		$order_info = $this->model_checkout_order->getOrder($this->session->data['order_id']);
		
		$this->data['order_id'] = str_pad($this->session->data['order_id'], 4, '0', STR_PAD_LEFT);
		$this->data['amount'] = 100 * $this->currency->format($order_info['total'], $order_info['currency_code'], '', FALSE);
        $this->data['currency'] = $order_info['currency_code'];
        
		$this->data['quickpay_cardtypelock'] = 'viabill';
        
		$this->data['quickpay_checksum'] = $this->createMd5('viabill');
		
		$this->id = 'payment';

		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/payment/quickpay_viabill.tpl')) {
			$this->template = $this->config->get('config_template') . '/template/payment/quickpay_viabill.tpl';
		} else {
			$this->template = 'default/template/payment/quickpay_viabill.tpl';
		}	
		
		$this->render();		
	}

	public function callback() {
		
        $callback_body = file_get_contents("php://input");
        $callback_json = json_decode( $callback_body );   

        $order_id = intval($callback_json->order_id);
        
        if( $order_id > 0) {
            $this->load->model('checkout/order');
            $transaction   = end( $callback_json->operations );

            $order_info = $this->model_checkout_order->getOrder($order_id);

            if ( $this->is_authorized_callback( $callback_body ) ) {
                if( $callback_json->accepted ) {
                    if( 'authorize' == $transaction->type OR 'capture' == $transaction->type) {
                        $this->log->write("Order_id = $order_id. Approved.");
                        $this->model_checkout_order->confirm($order_id, $this->config->get('quickpay_viabill_order_status_completed'));
                    }
                } else {
                    $this->log->write( 'The transaction for order #' . $order_id . ' was not accepted');
                }
            } else {
                $this->log->write("Order_id = $order_id. Checksum doesn't match.");
            }    
        } else {
            $this->log->write('No order found!');
        }
	}
    
  	/**
    
	* is_authorized_callback function.
	*
	* Performs a check on payment callbacks to see if it is legal or spoofed
	*
	* @access public
	* @return boolean
	*/  
    public function is_authorized_callback( $response_body ) 
    {
        if( ! isset( $_SERVER["HTTP_QUICKPAY_CHECKSUM_SHA256"] ) ) 
        {
            return FALSE;
        }
            
        return hash_hmac( 'sha256', $response_body, $this->config->get('quickpay_viabill_privatekey') ) == $_SERVER["HTTP_QUICKPAY_CHECKSUM_SHA256"];
    }
    
    
	protected function createMd5($cardtypelock = '') {
        $params = array(
            'agreement_id'      => $this->data['quickpay_viabill_agreement'],
            'merchant_id'       => $this->data['quickpay_viabill_merchant'],
            'language'          => $this->data['quickpay_viabill_language'],
            'order_id'          => $this->data['order_id'],
            'amount'            => $this->data['amount'],
            'currency'          => $this->data['currency'],
            'continueurl'       => $this->data['quickpay_viabill_continueurl'],
            'cancelurl'         => $this->data['quickpay_viabill_cancelurl'],
            'callbackurl'       => $this->data['quickpay_viabill_callbackurl'],
            'autocapture'       => $this->data['quickpay_viabill_autocapture'],
            'autofee'           => $this->data['quickpay_viabill_autofee'],
            'payment_methods'   => $this->data['quickpay_cardtypelock'],
            'version'			=> $this->data['quickpay_viabill_version']
        );

        ksort( $params );

        $checksum = hash_hmac("sha256", implode( " ", $params ), $this->config->get('quickpay_viabill_apikey'));
        
        return $checksum;
	}
}
?>