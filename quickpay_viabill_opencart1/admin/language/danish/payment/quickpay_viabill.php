<?php
// Heading
$_['heading_title'] = 'ViaBill Quickpay';

// Text 
$_['text_payment'] = 'Payment';
$_['text_success'] = 'Success: You have modified ViaBill/Quickpay Checkout account details!';
$_['text_quickpay_viabill'] = '<a onclick="window.open(\'http://viabill.com/\');"><img src="../catalog/view/viabillquickepay/viabill_logo.png" alt="ViaBill" title="ViaBill" style="border: 1px solid #EEEEEE;" /></a>';

$_['text_authorization'] = 'Authorization';
$_['text_sale'] = 'Sale';
$_['text_pricetagsrc'] = 'Price Tag Script';

// Entry
$_['entry_agreement'] = 'Agreement ID';
$_['entry_apikey'] = 'API Key';
$_['entry_privatekey'] = 'Private Key';
$_['entry_autofee'] = 'Auto-fee';
$_['entry_status'] = 'Status:';
$_['entry_merchant'] = 'Merchant ID:';
$_['entry_language'] = 'Language:';
$_['entry_md5check'] = 'MD5 Key:';
$_['entry_protocol'] = 'Protocol:';
$_['entry_msgtype'] = 'Msgtype:';
$_['entry_autocapture'] = 'Autocapture:';
$_['entry_splitpayment'] = 'Splitpayment:';
$_['entry_cardtypelock'] = 'Cardtypelock:';
$_['entry_order_status_completed'] = 'Ordre status ved gennemført ordre:';
$_['entry_secret'] = 'Secret Key:';
$_['entry_payment_methods'] = 'Betalings metoder:';
$_['entry_pricetagsrc'] = 'Price Tag Script:';

// Error
$_['error_permission']   = 'Advarsel! Du har ikke tilladelse til at ændre QuickPay!';
$_['error_merchant'] = 'Merchant:';
$_['error_continueurl'] = 'Continueurl:';
$_['error_cancelurl'] = 'Cancelurl:';
$_['error_callbackurl'] = 'Callbackurl:';
$_['error_secret'] = 'Secret:';
$_['error_protocol'] = 'Protocol:';
$_['error_msgtype'] = 'Msgtype:';
$_['error_cardtypelock'] = 'Cardtypelock:';

?>


