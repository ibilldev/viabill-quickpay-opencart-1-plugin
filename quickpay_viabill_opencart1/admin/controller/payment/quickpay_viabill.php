<?php 
class ControllerPaymentQuickPayViabill extends Controller {
	private $error = array(); 

	public function index() {
		$this->language->load('payment/quickpay_viabill');

		$this->document->setTitle($this->language->get('heading_title'));
		
		$this->load->model('setting/setting');
			
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && ($this->validate())) {
			$this->model_setting_setting->editSetting('quickpay_viabill', $this->request->post);				
			
			$this->session->data['success'] = $this->language->get('text_success');

			$this->redirect($this->url->link('extension/payment', 'token=' . $this->session->data['token'], 'SSL'));
		}

		$this->data['heading_title'] = $this->language->get('heading_title');

		$this->data['text_enabled'] = $this->language->get('text_enabled');
		$this->data['text_disabled'] = $this->language->get('text_disabled');
		$this->data['text_all_zones'] = $this->language->get('text_all_zones');
		$this->data['text_yes'] = $this->language->get('text_yes');
		$this->data['text_no'] = $this->language->get('text_no');
		$this->data['text_authorization'] = $this->language->get('text_authorization');
		$this->data['text_sale'] = $this->language->get('text_sale');
		
		$this->data['entry_status'] = $this->language->get('entry_status');
		$this->data['entry_privatekey'] = $this->language->get('entry_privatekey');
		$this->data['entry_agreement'] = $this->language->get('entry_agreement');
		$this->data['entry_apikey'] = $this->language->get('entry_apikey');
		$this->data['entry_merchant'] = $this->language->get('entry_merchant');
		$this->data['entry_pricetagsrc'] = $this->language->get('entry_pricetagsrc');

		$this->data['entry_language'] = $this->language->get('entry_language');
		$this->data['entry_autofee'] = $this->language->get('entry_autofee');
		$this->data['entry_autocapture'] = $this->language->get('entry_autocapture');
		$this->data['entry_order_status_completed'] = $this->language->get('entry_order_status_completed');
		
		$this->data['button_save'] = $this->language->get('button_save');
		$this->data['button_cancel'] = $this->language->get('button_cancel');
		
		
 		$errors = array('warning', 'agreement', 'autofee', 'apikey', 'privatekey', 'merchant', 'language' );
		foreach ($errors as $error) {
			if (isset($this->error[$error])) {
				$this->data['error_' . $error] = $this->error[$error];
			} else {
				$this->data['error_' . $error] = '';
			}
		}
		
		
		$this->data['breadcrumbs'] = array();
		
   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
      		'separator' => false
   		);
		
   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('text_payment'),
			'href'      => $this->url->link('extension/payment', 'token=' . $this->session->data['token'], 'SSL'),      		
      		'separator' => ' :: '
   		);
		
   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('payment/quickpay', 'token=' . $this->session->data['token'], 'SSL'),
      		'separator' => ' :: '
   		);
		
		$this->data['action'] = $this->url->link('payment/quickpay_viabill', 'token=' . $this->session->data['token'], 'SSL');
		
		$this->data['cancel'] = $this->url->link('extension/payment', 'token=' . $this->session->data['token'], 'SSL');
		
		
		
		$fields = array('quickpay_viabill_status', 
						'quickpay_viabill_agreement', 
						'quickpay_viabill_merchant', 
						'quickpay_viabill_language', 
						'quickpay_viabill_privatekey',
						'quickpay_viabill_pricetagsrc',
						'quickpay_viabill_apikey', 
						'quickpay_viabill_order_status_completed', 
						'quickpay_viabill_autocapture', 
						'quickpay_viabill_autofee');

		foreach ($fields as $field) {
			if (isset($this->request->post[$field])) {
				$this->data[$field] = $this->request->post[$field];
			} else {
				$this->data[$field] = $this->config->get($field);
			}
		}

		if (!isset($this->data['quickpay_viabill_status'])) $this->data['quickpay_viabill_status'] = "0";
		if (!isset($this->data['quickpay_viabill_language'])) $this->data['quickpay_viabill_language'] = "da";
		if (!isset($this->data['quickpay_viabill_order_status_completed'])) $this->data['quickpay_order_status_completed'] = "5";
		if (!isset($this->data['quickpay_viabill_autocapture'])) $this->data['quickpay_viabill_autocapture'] = "0";
        if (!isset($this->data['quickpay_creditcard_autofee'])) $this->data['quickpay_creditcard_autofee'] = "0";

		$this->data['languages'] = array('da' => 'Danish', 'de' => 'German', 'en' => 'English', 'fo' => 'Faeroese', 'fr' => 'French', 'kl' => ' Greenlandish', 'it' => 'Italian', 'no' => 'Norwegian', 'nl' => 'Dutch', 'pl' => 'Polish', 'ru' => 'Russian', 'sv' => 'Swedish');

		$this->load->model('localisation/order_status');
		
		$this->data['order_statuses'] = $this->model_localisation_order_status->getOrderStatuses();
		
		$this->template = 'payment/quickpay_viabill.tpl';
		$this->children = array(
			'common/header',	
			'common/footer'	
		);
		
		$this->response->setOutput($this->render());
	}

	private function validate() {
		if (!$this->user->hasPermission('modify', 'payment/quickpay_viabill')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}
		
		$errors = array('agreement', 'privatekey', 'apikey', 'merchant');
		foreach ($errors as $error) {
			if (!$this->request->post['quickpay_viabill_' . $error]) {
				$this->error[$error] = $this->language->get('error_' . $error);
			}
		}
		
		if (!$this->error) {
			return TRUE;
		} else {
			return FALSE;
		}	
	}
}
?>