<?php echo $header; ?>
<?php if ($error_warning) { ?>
<div class="warning"><?php echo $error_warning; ?></div>
<?php } ?>
<div class="box">
	<div class="left"></div>
	<div class="right"></div>
	<div class="heading">
		<h1 style="background-image: url('view/image/payment.png');"><?php echo $heading_title; ?></h1>
		<div class="buttons"><a onclick="$('#form').submit();" class="button"><span><?php echo $button_save; ?></span></a><a onclick="location = '<?php echo $cancel; ?>';" class="button"><span><?php echo $button_cancel; ?></span></a></div>
	</div>
	<div class="content">
		<form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form">
			<table class="form">
				<tr>
					<td><?php echo $entry_status; ?></td>
					<td>
						<select name="quickpay_viabill_status">
						<?php if ($quickpay_viabill_status) { ?>
							<option value="1" selected="selected"><?php echo $text_enabled; ?></option>
							<option value="0"><?php echo $text_disabled; ?></option>
						<?php } else { ?>
							<option value="1"><?php echo $text_enabled; ?></option>
							<option value="0" selected="selected"><?php echo $text_disabled; ?></option>
						<?php } ?>
						</select>
					</td>
				</tr>
				<tr>
					<td><span class="required">*</span> <?php echo $entry_merchant; ?></td>
					<td><input type="text" name="quickpay_viabill_merchant" value="<?php echo $quickpay_viabill_merchant; ?>" />
					<?php if ($error_merchant) { ?>
					<span class="error"><?php echo $error_merchant; ?></span>
					<?php } ?></td>
				</tr>
				<tr>
					<td><span class="required">*</span> <?php echo $entry_agreement; ?></td>
					<td><input type="text" name="quickpay_viabill_agreement" value="<?php echo $quickpay_viabill_agreement; ?>" />
					<?php if ($error_agreement) { ?>
					<span class="error"><?php echo $error_agreement; ?></span>
					<?php } ?></td>
				</tr>
				<tr>
					<td><span class="required">*</span> <?php echo $entry_apikey; ?></td>
					<td><input type="text" name="quickpay_viabill_apikey" value="<?php echo $quickpay_viabill_apikey; ?>" />
					<?php if ($error_apikey) { ?>
					<span class="error"><?php echo $error_apikey; ?></span>
					<?php } ?></td>
				</tr>
				<tr>
					<td><span class="required">*</span> <?php echo $entry_privatekey; ?></td>
					<td><input type="text" name="quickpay_viabill_privatekey" value="<?php echo $quickpay_viabill_privatekey; ?>" />
					<?php if ($error_privatekey) { ?>
					<span class="error"><?php echo $error_privatekey; ?></span>
					<?php } ?></td>
				</tr>
				<tr>
					<td><?php echo $entry_pricetagsrc; ?></td>
					<td><textarea name="quickpay_viabill_pricetagsrc" ><?php echo $quickpay_viabill_pricetagsrc; ?></textarea>
					
					</td>
				</tr>
				<tr>
					<td><span class="required">*</span> <?php echo $entry_language; ?></td>
					<td>
						<select name="quickpay_viabill_language">
						<?php
						foreach ($languages as $key => $value) {
							if ($key == $quickpay_viabill_language) { ?>
							<option value="<?php echo $key; ?>" selected="selected"><?php echo $value; ?></option>
							<?php } else { ?>
							<option value="<?php echo $key; ?>"><?php echo $value; ?></option>
						<?php }
						} ?>
						</select>
					</td>
				</tr>
				<tr>
					<td><?php echo $entry_autocapture; ?></td>
					<td>
						<?php if ($quickpay_viabill_autocapture) { ?>
							<input type="radio" name="quickpay_viabill_autocapture" value="1" checked="checked" />
							<?php echo $text_yes; ?>
							<input type="radio" name="quickpay_viabill_autocapture" value="0" />
							<?php echo $text_no;
							} else { ?>
							<input type="radio" name="quickpay_viabill_autocapture" value="1" />
							<?php echo $text_yes; ?>
							<input type="radio" name="quickpay_viabill_autocapture" value="0" checked="checked" />
							<?php echo $text_no;
						} ?>
						</select>
					</td>
				</tr>
				<tr>
					<td><?php echo $entry_autofee; ?></td>
					<td>
						<?php if ($quickpay_viabill_autofee) { ?>
							<input type="radio" name="quickpay_viabill_autofee" value="1" checked="checked" />
							<?php echo $text_yes; ?>
							<input type="radio" name="quickpay_viabill_autofee" value="0" />
							<?php echo $text_no;
							} else { ?>
							<input type="radio" name="quickpay_viabill_autofee" value="1" />
							<?php echo $text_yes; ?>
							<input type="radio" name="quickpay_viabill_autofee" value="0" checked="checked" />
							<?php echo $text_no;
						} ?>
						</select>
					</td>
				</tr>
				<tr>
					<td><?php echo $entry_order_status_completed; ?></td>
					<td>
						<select name="quickpay_viabill_order_status_completed">
						<?php
						foreach ($order_statuses as $order_status) {
							if ($order_status['order_status_id'] == $quickpay_viabill_order_status_completed) { ?>
							<option value="<?php echo $order_status['order_status_id']; ?>" selected="selected"><?php echo $order_status['name']; ?></option>
							<?php } else { ?>
							<option value="<?php echo $order_status['order_status_id']; ?>"><?php echo $order_status['name']; ?></option>
						<?php }
						} ?>
						</select>
					</td>
				</tr>
			</table>
		</form>
	</div>
</div>
<?php echo $footer; ?>