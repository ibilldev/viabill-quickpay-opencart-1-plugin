### Opencart2 : ViaBill QuickPay Module ###
----------------------  

ViaBill has developed a free payment module using QuickPay Payment Gateway which enables your customers to pay online for their orders in your Opencart 1 Web Shop.
ViaBill is a Payment Method and not a Payment Gateway.

###Facts###
---------
- version: 1.1
- Plugin on BitBucket (https://pdviabill@bitbucket.org/ibilldev/viabill-quickpay-opencart-1-plugin.git)


###Description###
-----------
Pay using ViaBill. 
Install this Plugin in to your Opencart1 Web Shop to provide a separate payment option to pay using ViaBill.

#Requirements
------------
* PHP >= 5.2.0

#Compatibility
-------------
* Opencart 1


###Integration Instructions###
-------------------------
1. Download the Module from the bitbucket. 

2. Module contains two folder a.) admin b.) catalog 

Please follow the following folder structure to place the relevant files for ViaBill QuickPay Integration:
![opencart1-qp-admin.PNG](https://bitbucket.org/repo/L49Eb9/images/1919984069-opencart1-qp-admin.PNG)
![opencart1-qp-catalog.PNG](https://bitbucket.org/repo/L49Eb9/images/3692593314-opencart1-qp-catalog.PNG)

3. Copy the content of admin folder inside your project's admin folder(Follow the folder structure)

4. Copy the content of catalog folder inside your project's catalog folder(Follow the folder structure)

5. Go to the Admin-> Extensions-> Payments. Look for "ViaBill Quickpay "

6. Click on Install.

7. Now edit on the plugin.

8. Enable/Disable :  Please Enable the Status. 

9. Enter Merchant ID/Agreement ID/ API Key/ Private Key/Language

10. Save and Done.


##Uninstallation/Disable Module
-----------------------
1. Go to the Admin->Extensions->Payments
2. Look For "ViaBill Quickpay Payment Option"
3. Choose Option to Uninstall


#Support
-------
If you have any issues with this extension, kindly drop us a mail on [support@viabill.com](mailto:support@viabill.com)

#Contribution
------------


#License
-------
[OSL - Open Software Licence 3.0](http://opensource.org/licenses/osl-3.0.php)